import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.lang.Math;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class Trie {
	private HashMap<Node, Node> nodes = new HashMap<Node, Node>();
	private String[] strings = new String[0];
	private Node root = new Node();

	class Node {
		private Object[] children = new Object[0];
		private boolean isEnd = false;

		private void addChild(String key, Node value) {
			Object[] nc = new Object[children.length + 2];
			System.arraycopy(children, 0, nc, 0, children.length);
			nc[children.length] = dedup(key);
			nc[children.length + 1] = value;
			children = nc;
		}

		private Node getChild(String key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; i += 2)
				if (children[i].equals(key))
					return (Node) children[i + 1];
			return null;
		}
	}

	private String dedup(String s) { // This is for deduplicating the index strings.
		for (String i : strings)
			if (i.equals(s))
				return i;
		String[] ns = new String[strings.length + 1];
		System.arraycopy(strings, 0, ns, 0, strings.length);
		ns[strings.length] = s;
		strings = ns;
		return s;
	}

	public void add(String s) {
		Node n = root;
		while (s.length() > 0) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			Node nn = n.getChild(c);
			if (nn == null) {
				nn = new Node();
				n.addChild(c, nn);
			}
			n = nn;
		}
		n.isEnd = true;
	}

	public boolean get(String s) {
		Node n = root;
		while (s.length() > 0 && n != null) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			n = n.getChild(c);
		}
		return n != null && n.isEnd;
	}
}

public class Dictionary4 {
	private static Trie trie = new Trie();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				trie.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		/*for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}*/
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (trie.get(word))
					System.out.println("Valid query.");
				else System.out.println("Nope!");
				/*for (FuzzyMap.Item it = map.get(word); it != null; it = it.next) {
					System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
					if (++i >= 15)
						break;
				}*/
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
