import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;

import java.io.InputStreamReader; // TODO this is just for debug

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class Dafsa {
	private class Node { // TODO How to remove words, keep refcounts?
		// TODO HashMap and ArrayList may not be ideal choises.
		private HashSet<Node> refs = new HashSet<Node>(); // TODO Check if there's no (reverse) dead ends!
		private HashMap<String, Node> links = new HashMap<String, Node>();

		private Node copy() {
			Node ret = new Node();
			ret.links.putAll(links);
			for (Node l : links.values())
				l.refs.add(ret); // Add a ref for each link.
			return ret;
		}

		private void link(String k, Node n) {
			links.put(k, n);
			n.refs.add(this);
		}

		private void cleanup() {
			if (refs.size() == 0) {
				for (Node l : links.values()) {
					l.refs.remove(this);
					l.cleanup(); // TODO Stop recursing!
				}
			}
		}

		private void merge(Node n) {
			// Remove all refs to n.
			for (Node l : n.links.values())
				l.refs.remove(n);
			// Change all links to n to this.
			for (Node r : n.refs) {
				for (String k : r.links.keySet()) {
					if (r.links.get(k) == n) {
						r.links.put(k, this);
					}
				}
			}
			for (Node r : refs) {
				for (String k : r.links.keySet()) {
					if (r.links.get(k) == n) {
						r.links.put(k, this);
					}
				}
			}
		}

		public void add(String word) {
			if (word.length() == 0) { // End of word.
				if (links.get("") != end) {
					links.put("", end);
					end.refs.add(this);
				}
				// Now we should have a chain back to the root with only one ref for each node.
				Node n = end, c = this;
				boolean e = false;
				while (e == false) {
					e = true;
					//System.out.println("Refs " + Integer.toString(n.refs.size()) + " " + Boolean.toString(n == end));
					for (Node t : n.refs) {
						if (t == c)
							continue;
						if (t.links.equals(c.links)) {
							t.merge(c);
							c = (Node) c.refs.toArray()[0];
							//c = c.refs.get(0);
							n = t;
							e = false;
							break;
						}
					}
				}
				// TODO Make sure there is an end ref to us, there should only be one if we merged.
				return;
			}
			String c = word.substring(0, 1);
			Node sub = links.get(c);
			// TODO don't recurse or limit depth (note also, java does not do tail call optimization)
			if (sub == null) { // New node.
				sub = new Node();
				link(c, sub);
				sub.add(word.substring(1));
				return;
			}
			// Existing node.
			Node nsub = sub.copy(); // Copy it so we don't wrongly add a suffix (it could be linked from somewhere else).
			link(c, nsub);
			if (!links.containsValue(sub))
				sub.refs.remove(this); // Remove ref from old sub, if it was the only one.
			nsub.add(word.substring(1));
			sub.cleanup(); // Delete if references are gone.
		}

		public boolean check(String word) {
			if (word.length() == 0) {
				if (links.get("") == end)
					return true;
				return false;
			}
			String c = word.substring(0, 1);
			Node sub = links.get(c);
			//System.out.println("sub " + sub);
			// TODO don't recurse or limit depth (note also, java does not do tail call optimization)
			if (sub != null)
				return sub.check(word.substring(1));
			return false;
		}

		// TODO this does not belong here
		private int maxdel = 3;
		private int minlen = 1;

		private void _find(String word, String result, HashSet<String> ret, Node start, int ndel) {
			if (ndel < maxdel) {
				// Try with a delete.
				start.links.forEach((k, v) ->
					_find(word, result + k, ret, v, ndel + 1)
				);
			}
			// Try without a delete.
			// TODO Note that this does pull the reduntant item from above but ndel being less is important.
			if (word.length() == 0) {
				if (start.links.get("") == end) {
					/*System.out.println("result " + result);*/
					ret.add(result);
					return;
				}
				return;
			}
			String c = word.substring(0, 1);
			Node sub = start.links.get(c);
			//System.out.println("sub " + sub);
			// TODO don't recurse or limit depth (note also, java does not do tail call optimization)
			if (sub != null)
				_find(word.substring(1), result + word.substring(0, 1), ret, sub, ndel);
		}

		public HashSet<String> find(String word) {
			HashSet<String> ret = new HashSet<String>();
			_find(word, "", ret, this, 0);
			return ret;
		}
	}

	private Node root = new Node();
	private Node end = new Node();

	public void add(String word) {
		root.add(word);
	}

	public boolean check(String word) {
		return root.check(word);
	}

	public HashSet<String> find(String word) {
		return root.find(word);
	}
}

// TODO remember this does not take into account upper/lowercase
class FuzzyMap {
	private static int maxdist = 3; // TODO make configurable / choose wise defaults
	private static int maxdel = 3;
	private static int minlen = 1;
	private Dafsa d = new Dafsa();

	public class Item {
		public String value;
		public int dist;
		Item next;
	}

	// TODO allow limit items count (may need to look in fget also/instead)
	// TODO sort by popularity
	// TODO maybe do not sort here so we can do weighted distance vs popularity sort
	// TODO how to deal with sorting user items that may be more popular than the normal dictionary?
	private Item _get(String key, String keyOrig, Item ret, int del, int skip) { /* Note that this could return null. */
		/* Convert and sort relevant items from the MHashMap return. */
		HashSet<String> hs = d.find(key);
		for (String it : hs) {
			Item nit = new Item(); /* Copy the item. */
			nit.value = it;
			nit.dist = distance(nit.value, keyOrig);
			nit.next = null;
			if (nit.dist > maxdist) /* Skip if DL distance is too far. */
				continue;
			/* Insert before items more distant, we append since we're looping to check for duplicates anyway. */
			if (ret == null) /* No target, place initial item. */
				ret = nit;
			else if (ret.dist > nit.dist) { /* Shortest distance yet, insert at start. */
				nit.next = ret;
				ret = nit;
			} else for (Item c = ret;; c = c.next) { /* Iterate through existing items. */
				if (c.value.equals(nit.value)) /* Stop if exists. */
					break;
				if (c.next == null) { /* This is the last item and ours did not exist, add it. */
					c.next = nit;
					break;
				}
				if (c.next.dist > nit.dist) { /* We know c.next is non-null because above code. */
					nit.next = c.next;
					c.next = nit;
					break;
				}
			}
		}
		/* Get with deletes in key. */
		int len = key.length();
		if (del == maxdel || len <= minlen)
			return ret;
		for (int i = skip; i < len; ++i) { /* Get with deletes in key. */
			String part = key.substring(0, i) + key.substring(i + 1);
			ret = _get(part, keyOrig, ret, del + 1, i);
		}
		return ret;
	}

	public Item get(String key) {
		return _get(key, key, null, 0, 0);
	}

	public void add(String value) {
		d.add(value);
	}

	/* Calculate restricted (optimal string alignment) Damerau-Levenshtein distance using modified Wagner-Fischer algorithm.
	 *
	 * Restricted  in this context means transpositions only count if there are no subsequent edits on their letters.
	 * For more information see (as loaded on 2019-09-27):
	 *   https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Definition
	 *   https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0
	 */
	private static int distance(String a, String b) { // TODO shud be private
		int la = a.length(), lb = b.length();
		int[][] d = new int[la + 1][lb + 1]; /* We have an extra row and column which is 0 when pretending a and b are 1-indexed. */
		for (int i = 0; i <= la; ++i) /* If j == 0 the distance is i. */
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) /* If i == 0 the distance is j, and (0, 0) is already set. */
			d[0][j] = j;
		for (int i = 1; i <= la; ++i) {
			for (int j = 1; j <= lb; ++j) {
				d[i][j] = d[i - 1][j] + 1;
				d[i][j] = Math.min(d[i][j], d[i][j - 1] + 1);
				d[i][j] = Math.min(d[i][j], d[i - 1][j - 1] + ((a.charAt(i - 1) != b.charAt(j - 1)) ? 1 : 0));
				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1))
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
			}
		}
		return d[la][lb];
	}
}

public class Test {
	static FuzzyMap map = new FuzzyMap();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null) {
				map.add(line);
			}
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		/*for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}*/
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));
		
		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				for (FuzzyMap.Item it = map.get(word); it != null; it = it.next) {
					System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
					if (++i >= 15)
						break;
				}
				System.out.println("-- end result --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}

