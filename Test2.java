import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Arrays;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

// For bloom filter.
import java.lang.Math;
import java.util.BitSet;
import java.util.Random;

class Bloom {
	private BitSet bits;
	private short k;
	private static Random prng = new Random();

	/* p = target probability of false positives */
	/* n = expected number of items */
	/* See also https://hur.st/bloomfilter/ */
	public Bloom(double p, int n) {
		int m = (int) Math.ceil((n * Math.log(p)) / Math.log(1 / Math.pow(2, Math.log(2))));;
		k = (short) Math.round(Math.log(2) * m / n);
		bits = new BitSet(m);
	}

	public void add(Object o) {
		prng.setSeed(o.hashCode());
		for (int i = 0; i < k; ++i)
			bits.set(Math.abs(prng.nextInt()) % bits.size());
	}

	public boolean contains(Object o) {
		prng.setSeed(o.hashCode());
		for (int i = 0; i < k; ++i)
			if (!bits.get(Math.abs(prng.nextInt()) % bits.size()))
				return false;
		return true;
	}
}

class BloomDict {
	private ArrayList<String> words = new ArrayList<String>();
	private ArrayList<Bloom> bloom = new ArrayList<Bloom>();
	private int nsb = 128;
	private Bloom[] sb = new Bloom[nsb];

	public BloomDict() {
		for (int i = 0; i < nsb; ++i)
			sb[i] = new Bloom(0.001, 100000);
	}

	public void add(HashSet<String> keys, String w) {
		words.add(w);
		Bloom b = new Bloom(0.001, keys.size()); // TODO Select size wisely.
		for (String key : keys) {
			b.add(key);
			sb[(words.size() - 1) % nsb].add(key);
		}
		bloom.add(b);
	}

	public ArrayList<String> get(String w) {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < nsb; ++i) {
			if (sb[i].contains(w)) {
				for (int j = i; j < bloom.size(); j += nsb) {
					if (bloom.get(j).contains(w))
						ret.add(words.get(j));
				}
			}
		}
		/*for (int i = 0; i < bloom.size(); ++i) {
			if (bloom.get(i).contains(w))
				ret.add(words.get(i));
		}*/
		//System.out.println(Integer.toString(ret.size()));
		return ret;
	}
}

// TODO remember this does not take into account upper/lowercase
class FuzzyMap {
	private static int maxdist = 3; // TODO make configurable / choose wise defaults
	private static int maxdel = 3;
	private static int minlen = 1;
	private BloomDict bd = new BloomDict();

	public class Item {
		public String value;
		public int dist;
		Item next;
	}

	private static HashSet<String> _getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		int[] its = new int[depth];
		while (its[depth - 1] + depth - 1 < str.length()) {
			String s = str;
			for (int i = 0; i < depth; ++i) /* Note that the order matters. */
				s = s.substring(0, its[i]) + s.substring(its[i] + 1);
			ret.add(s);
			++its[0];
			for (int i = 1; i < depth; ++i) {
				if (its[i] < its[i - 1]) {
					its[i - 1] = 0;
					++its[i];
				} else break;
			}
		}
		return ret;
	}

	private static HashSet<String> getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		for (int i = 1; i <= depth; ++i)
			if (str.length() > i) // TODO maybe consider minlen, or just hardcode this way?
				ret.addAll(_getpartials(str, i));
		return ret;
	}

	// TODO allow limit items count (may need to look in fget also/instead)
	// TODO sort by popularity
	// TODO maybe do not sort here so we can do weighted distance vs popularity sort
	// TODO how to deal with sorting user items that may be more popular than the normal dictionary?
	private Item _get(String key, String keyOrig, Item ret, int del, int skip) { /* Note that this could return null. */
		/* Convert and sort relevant items from the MHashMap return. */
		ArrayList<String> res = bd.get(key);
		for (String it : res) {
			Item nit = new Item(); /* Copy the item. */
			nit.value = it;
			nit.dist = distance(nit.value, keyOrig);
			nit.next = null;
			if (nit.dist > maxdist) /* Skip if DL distance is too far. */
				continue;
			/* Insert before items more distant, we append since we're looping to check for duplicates anyway. */
			if (ret == null) /* No target, place initial item. */
				ret = nit;
			else if (ret.dist > nit.dist) { /* Shortest distance yet, insert at start. */
				nit.next = ret;
				ret = nit;
			} else for (Item c = ret;; c = c.next) { /* Iterate through existing items. */
				if (c.value.equals(nit.value)) /* Stop if exists. */
					break;
				if (c.next == null) { /* This is the last item and ours did not exist, add it. */
					c.next = nit;
					break;
				}
				if (c.next.dist > nit.dist) { /* We know c.next is non-null because above code. */
					nit.next = c.next;
					c.next = nit;
					break;
				}
			}
		}
		/* Get with deletes in key. */
		int len = key.length();
		if (del == maxdel || len <= minlen)
			return ret;
		for (int i = skip; i < len; ++i) { /* Get with deletes in key. */
			String part = key.substring(0, i) + key.substring(i + 1);
			ret = _get(part, keyOrig, ret, del + 1, i);
		}
		return ret;
	}

	public Item get(String key) {
		return _get(key, key, null, 0, 0);
	}

	public void add(String value) {
		HashSet<String> keys = getpartials(value, maxdel);
		keys.add(value);
		bd.add(keys, value);
	}

	/* Calculate restricted (optimal string alignment) Damerau-Levenshtein distance using modified Wagner-Fischer algorithm.
	 *
	 * Restricted  in this context means transpositions only count if there are no subsequent edits on their letters.
	 * For more information see (as loaded on 2019-09-27):
	 *   https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Definition
	 *   https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0
	 */
	private static int distance(String a, String b) { // TODO shud be private
		int la = a.length(), lb = b.length();
		int[][] d = new int[la + 1][lb + 1]; /* We have an extra row and column which is 0 when pretending a and b are 1-indexed. */
		for (int i = 0; i <= la; ++i) /* If j == 0 the distance is i. */
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) /* If i == 0 the distance is j, and (0, 0) is already set. */
			d[0][j] = j;
		for (int i = 1; i <= la; ++i) {
			for (int j = 1; j <= lb; ++j) {
				d[i][j] = d[i - 1][j] + 1;
				d[i][j] = Math.min(d[i][j], d[i][j - 1] + 1);
				d[i][j] = Math.min(d[i][j], d[i - 1][j - 1] + ((a.charAt(i - 1) != b.charAt(j - 1)) ? 1 : 0));
				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1))
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
			}
		}
		return d[la][lb];
	}
}

public class Test2 {
	static FuzzyMap map = new FuzzyMap();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				map.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));
		
		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				for (FuzzyMap.Item it = map.get(word); it != null; it = it.next) {
					System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
					if (++i >= 15)
						break;
				}
				System.out.println("-- end results --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
