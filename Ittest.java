import java.util.HashSet;

public class Ittest {
	private static HashSet<String> _getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		int[] its = new int[depth];
		while (its[depth - 1] + depth - 1 < str.length()) {
			String s = str;
			for (int i = 0; i < depth; ++i) /* Note that the order matters. */
				s = s.substring(0, its[i]) + s.substring(its[i] + 1);
			ret.add(s);
			++its[0];
			for (int i = 1; i < depth; ++i) {
				if (its[i] < its[i - 1]) {
					its[i - 1] = 0;
					++its[i];
				} else break;
			}
		}
		return ret;
	}

	private static HashSet<String> getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		for (int i = 1; i <= depth; ++i)
			if (str.length() > i) // TODO maybe consider minlen, or just hardcode this way?
				ret.addAll(_getpartials(str, i));
		return ret;
	}

	public static void main(String[] args) {
		HashSet<String> l = getpartials("polytetrafluoroethylene", 3);
		for (String s : l)
			System.out.println(s);
		System.out.println(l.size());
	}
}
