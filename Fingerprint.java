import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class Dictionary {
	private HashMap<Integer, HashSet<String>> map = new HashMap<Integer, HashSet<String>>();

	private int fingerprint(String w) { // TODO unicode
		int ret = 0;
		for (int i = 0, l = w.length(); i < l; ++i)
			ret |= 1 << (w.charAt(i) % 32); // Using OR appears to give lower maximum bucket sizes than XOR.
		return ret;
	}

	public void add(String w) {
		int fp = fingerprint(w);
		HashSet<String> hs = map.get(fp);
		if (hs == null)
			hs = new HashSet<String>();
		hs.add(w);
		map.put(fp, hs);
	}

	public HashSet<String> search(String w) {
		HashSet<String> ret = new HashSet<String>();
		int fp = fingerprint(w);
		HashSet<String> hs = map.get(fp);
		if (hs != null)
			ret.addAll(hs);

		for (int ptl : ptls) {
			if (((fp ^ ptl) & fp) == 0)
				continue; // Require at least 1 char in common.
			hs = map.get(fp ^ ptl);
			if (hs != null)
				ret.addAll(hs);
		}

		return ret;
	}

	ArrayList<Integer> ptls = getpartials(3);

	public void printstuff() {
		System.out.println("dsz = " + map.size());
		Iterator it = map.entrySet().iterator();
		int max = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			int sz = ((HashSet<String>) pair.getValue()).size();
			max = ((sz > max) ? sz : max);
		}
		System.out.println("max = " + max);
		System.out.println("ptls = " + ptls.size());
	}

	private static ArrayList<Integer> _getpartials(int depth) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		int[] its = new int[depth];
		while (its[depth - 1] + depth - 1 < 32) {
			int s = 0;
			for (int i = 0; i < depth; ++i) /* Note that the order matters. */
				s |= 1 << (its[i] - i);
			ret.add(s);
			++its[0];
			for (int i = 1; i < depth; ++i) {
				if (its[i] < its[i - 1]) {
					its[i - 1] = 0;
					++its[i];
				} else break;
			}
		}
		return ret;
	}

	private static ArrayList<Integer> getpartials(int depth) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for (int i = 1; i <= depth; ++i)
			ret.addAll(_getpartials(i));
		return ret;
	}
}

public class Fingerprint {
	private static Dictionary dict = new Dictionary();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				dict.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		dict.printstuff();
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		/*for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}*/
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				/*if (trie.get(word))
					System.out.println("Valid query.");
				else System.out.println("Nope!");*/
				HashSet<String> words = dict.search(word);
				int ct = 0;
				for (String it : words) {
					if (it.length() <= word.length() + 3 && it.length() >= word.length() - 3) { // TODO just to know how much length matters
						System.out.println("lup = " + it);
						++ct;
					}
				}
				System.out.println("-- end, " + words.size() + " " + ct + " hits --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
