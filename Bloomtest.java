import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.Arrays;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

// For bloom filter.
import java.lang.Math;
import java.util.BitSet;
import java.util.Random;

class Bloom {
	private BitSet bits;
	private int k, m;
	private static Random prng = new Random();

	/* m = filter size in bits */
	/* n = expected number of items */
	/* See also https://hur.st/bloomfilter/ */
	public Bloom(int m, int n) {
		bits = new BitSet(m);
		k = (int) Math.round(Math.log(2) * m / n);
		this.m = m;
	}

	public void add(Object o) {
		prng.setSeed(o.hashCode());
		for (int i = 0; i < k; ++i)
			bits.set(Math.abs(prng.nextInt()) % m);
	}

	public boolean contains(Object o) {
		prng.setSeed(o.hashCode());
		for (int i = 0; i < k; ++i)
			if (!bits.get(Math.abs(prng.nextInt()) % m))
				return false;
		return true;
	}
}

public class Bloomtest {
	static Bloom bloom = new Bloom(14377588, 1000000);

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null) {
				bloom.add(line);
			}
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		loadfile("en_full.txt");
		System.out.println("-- ready --");
		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (bloom.contains(word))
					System.out.println("Word found!");
				else System.out.println("Nope.");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
