import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

class Util {
	public static int fnv32(int d) { // Calculate 32bit FNV-1a hash of 32bit number.
		int ret = 0x811C9DC5;
		ret = (ret ^ ((d >>>  0) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>>  8) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>> 16) & 0xFF)) * 0x1000193;
		ret = (ret ^ ((d >>> 24) & 0xFF)) * 0x1000193;
		return ret;
	}

	public static int[] stringToCodePoints(String s) {
		int[] ret = new int[s.codePointCount(0, s.length())];
		for (int i = 0; i < ret.length; ++i)
			ret[i] = s.codePointAt(s.offsetByCodePoints(0, i));
		return ret;
	}
}

@SuppressWarnings("unchecked")
class MHashMap<K, V> {
	private Object[] keys = new Object[256];
	private Object[] data = new Object[256];
	private int size = 0;

	private int gethc(K key) {
		int h = Math.abs(Util.fnv32(key.hashCode())) % keys.length; // The fnv part compensates for bad hashCode implementations.
		while (keys[h] != null && !keys[h].equals(key))
			h = (h + 1) % keys.length;
		return h;
	}

	public void put(K key, V value) {
		int h = gethc(key);
		if (keys[h] == null)
			++size;
		if (data.length * 8 / 10 < size + 1) { // Double the array if load factor is over 0.8.
			Object[] okeys = keys;
			Object[] odata = data;
			keys = new Object[keys.length * 2];
			data = new Object[data.length * 2];
			size = 0;
			for (int i = 0; i < okeys.length; ++i)
				if (okeys[i] != null)
					put((K) okeys[i], (V) odata[i]);
			h = gethc(key);
		}
		keys[h] = key;
		data[h] = value;
	}

	public V get(K key) {
		return (V) data[gethc(key)];
	}

	public void remove(K key) {
		int hs = gethc(key);
		if (keys[hs] != null) {
			keys[hs] = null;
			data[hs] = null;
			// Make sure all chained items are still reachable.
			for (int h = (hs + 1) % keys.length; keys[h] != null; h = (h + 1) % keys.length) {
				if (gethc((K) keys[h]) == hs) {
					keys[hs] = keys[h];
					data[hs] = data[h];
					keys[h] = null;
					data[h] = null;
					hs = h;
				}
			}
			--size;
		}
	}
}

@SuppressWarnings("unchecked")
class DedupMap<T> {
	private Object[] data = new Object[256];
	private int size = 0;

	private int gethc(T obj, boolean exact, boolean equiv) {
		int h = Math.abs(Util.fnv32(obj.hashCode())) % data.length; // The fnv part compensates for bad hashCode implementations.
		while (data[h] != null && !((exact && data[h] == obj) || (equiv && data[h].equals(obj) && data[h] != obj)))
			h = (h + 1) % data.length;
		return h;
	}

	public T add(T obj, boolean exact, boolean equiv) { // The exact and equiv determine when to get instead of add.
		int h = gethc(obj, exact, equiv);
		if (data[h] != null)
			return (T) data[h];
		if (data.length * 8 / 10 < size + 1) { // Double the array if load factor is over 0.8.
			Object[] odata = data;
			data = new Object[data.length * 2];
			size = 0;
			for (Object o : odata)
				if (o != null)
					add((T) o, false, false);
			h = gethc(obj, exact, equiv);
		}
		data[h] = obj;
		++size;
		return obj;
	}

	public T get(T obj, boolean exact, boolean equiv) {
		return (T) data[gethc(obj, exact, equiv)];
	}

	public void remove(T obj, boolean exact, boolean equiv) {
		int hs = gethc(obj, exact, equiv);
		if (data[hs] != null) {
			data[hs] = null;
			// Make sure all chained items are still reachable.
			for (int h = (hs + 1) % data.length; data[h] != null; h = (h + 1) % data.length) {
				if (gethc((T) data[h], true, false) == hs) {
					data[hs] = data[h];
					data[h] = null;
					hs = h;
				}
			}
			--size;
		}
	}
}

class Dawg {
	private MHashMap<String, Integer> scores = new MHashMap<String, Integer>(); // TODO The char and string eat a lot of ram.
	private DedupMap<Node> nodes = new DedupMap<Node>(); // TODO Word score follows an exponential curve, if we can keep them in order that could be good enough.
	private Node root = new Node();

	public class Result {
		public String word;
		public int score, dist;
		public boolean isPrefix;
	}

	private class Node {
		private Node[] children = new Node[0]; // TODO I think having two arrays eats significantly more memory, verify this (60.54 vs 51.45).
		private int[] children_idx = new int[0];
		private boolean isEnd = false; // TODO Just check words list hashmap instead of this?
		private int refc = 0; // TODO This is only needed while building table, before finish.

		private void incRefc() {
			if (++refc == 1)
				nodes.add(this, true, false);
		}

		private void decRefc() { // Warning, this could potentially overflow the stack (it's recursive).
			if (--refc == 0) {
				nodes.remove(this, true, false);
				for (Node c : children)
					c.decRefc();
			}
		}

		public void setChild(int key, Node value) { // Warning, potential stack overflow and this changes the hashCode.
			nodes.remove(this, true, false);
			value.incRefc(); // If we are overwriting the same value, this is decremented later and prevents refc falling to 0 in that case.
			for (int i = 0; i < children.length; ++i) {
				if (children_idx[i] == key) {
					children[i].decRefc();
					children[i] = value;
					nodes.add(this, true, false);
					return;
				}
			}
			children_idx = Arrays.copyOf(children_idx, children.length + 1);
			children = Arrays.copyOf(children, children.length + 1);
			children_idx[children.length - 1] = key;
			children[children.length - 1] = value;
			nodes.add(this, true, false);
		}

		public void setIsEnd(boolean v) {
			nodes.remove(this, true, false);
			isEnd = v;
			nodes.add(this, true, false);
		}

		public Node getChild(int key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; ++i)
				if (children_idx[i] == key)
					return children[i];
			return null;
		}

		public Node copy() {
			Node ret = new Node();
			ret.children = Arrays.copyOf(children, children.length);
			ret.children_idx = Arrays.copyOf(children_idx, children.length);
			ret.isEnd = isEnd;
			for (Node c : children)
				c.incRefc();
			return ret;
		}

		private void _getPfx(LinkedList<Result> ret, String r, int dist) {
			if (isEnd) {
				Result res = getResult(r, dist, true);
				if (res != null)
					ret.add(res);
			}
			getPfx(ret, r, dist);
		}

		public void getPfx(LinkedList<Result> ret, String r, int dist) { // Warning, potential stack overflow.
			for (int i = 0; i < children.length; ++i)
				children[i]._getPfx(ret, r + new String(children_idx, i, 1), dist); // TODO Should we use codepoint array instead?
		}

		@Override
		public int hashCode() {
			int ret = 0;
			for (int i = 0; i < children.length; ++i) // Must be order-independent!
				ret ^= System.identityHashCode(children[i]) + children_idx[i];
			return ret ^ (isEnd ? 1234 : 4321);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			Node n = (Node) o;
			if (isEnd != n.isEnd)
				return false;
			if (children.length != n.children.length)
				return false;
			for (int i = 0; i < children.length; ++i) { // Compares array regardless of order.
				boolean found = false;
				for (int j = 0; j < n.children.length; ++j)
					if (children[i] == n.children[j] && children_idx[i] == n.children_idx[j])
						found = true;
				if (!found)
					return false;
			}
			return true;
		}
	}

	public Result getResult(String word, int dist, boolean isPrefix) {
		Integer score = scores.get(word);
		if (score == null)
			return null;
		Result res = new Result();
		res.word = word;
		res.score = score;
		res.dist = dist;
		res.isPrefix = isPrefix;
		return res;
	}

	/* Insert similarly to https://doi.org/10.1016/S0304-3975(02)00571-6 */
	/* But with DedupMap to speed up finding similar nodes. */
	public void add(String str, int score) {
		ArrayList<Node> st = new ArrayList<Node>();
		int[] s = Util.stringToCodePoints(str);
		Node n = root;
		/* Split off existing nodes and create new ones. */
		st.add(n);
		for (int i = 0; i < s.length; ++i) {
			Node nn = n.getChild(s[i]);
			if (nn == null) {
				nn = new Node();
				n.setChild(s[i], nn);
				n = nn;
			} else {
				nn = nn.copy(); // We must copy it, otherwise we will mess up other words.
				n.setChild(s[i], nn);
				n = nn;
			}
			st.add(n);
		}
		n.setIsEnd(true);
		/* In reverse order, replace them with equivalent nodes. */
		for (int i = st.size() - 1; i > 0; --i) {
			Node on = st.get(i);
			if ((n = nodes.get(on, false, true)) != null) {
				Node p = st.get(i - 1);
				p.setChild(s[i - 1], n);
			}
		}
		Integer osc = scores.get(str);
		if (osc == null || osc < score) // Only add if score is higher than existing.
			scores.put(str, score);
	}

	public void finish() { // Stop ability to add but reduce memory use.
		nodes = null;
	}

	/* Used only by find function. */
	private void _find(LinkedList<Result> ret, Node n, int[][] d, int[] a, int[] b, int la, int lb, int dist, boolean pfx) {
		for (int j = 1; j <= lb; ++j) {
			d[la][j] = d[la - 1][j] + 1;
			d[la][j] = Math.min(d[la][j], d[la][j - 1] + 1);
			d[la][j] = Math.min(d[la][j], d[la - 1][j - 1] + ((a[la - 1] != b[j - 1]) ? 1 : 0));
			if (la > 1 && j > 1 && a[la - 1] == b[j - 2] && a[la - 2] == b[j - 1])
				d[la][j] = Math.min(d[la][j], d[la - 2][j - 2] + 1);
		}
		int mindist = dist + 1;
		for (int i = Math.max(0, la - dist), e = Math.min(lb, la + dist); i <= e; ++i)
			mindist = Math.min(mindist, d[la][i]);
		if (mindist <= dist) {
			for (int i = 0; i < n.children.length; ++i) {
				a[la] = n.children_idx[i];
				_find(ret, n.children[i], d, a, b, la + 1, lb, dist, pfx);
			}
		}
		if (d[la][lb] == dist) { // Use <= to get results up to a particular distance.
			if (n.isEnd) {
				Result res = getResult(new String(a, 0, la), d[la][lb], false);
				if (res != null)
					ret.add(res);
			}
			if (pfx)
				n.getPfx(ret, new String(a, 0, la), d[la][lb]);
		}
	}

	/* Search based on modified Wagner-Fischer algorithm. */
	/* Distance used is restricted (optimal string alignment) Damerau-Levenshtein distance. */
	public LinkedList<Result> find(String s, int dist, boolean pfx) { // Warning, recursive, catch stackoverflow.
		LinkedList<Result> ret = new LinkedList<Result>(); // TODO use arraylist if no sorting done here yet
		int[] b = Util.stringToCodePoints(s); // TODO How to deal with capitalization? Maybe store map with only words that have alternate capitalization / chars
		int[] a = new int[b.length + dist + 1]; // TODO The problem with capitals is have to fetch the capitalized word from dict. i guess. Or maybe just consider incorrect capitals as any other mistake?
		int[][] d = new int[b.length + dist + 2][b.length + 1]; // The max for la is b.length() + di.
		for (int i = 0; i <= b.length + dist + 1; ++i) // If j == 0 the distance is i.
			d[i][0] = i;
		for (int j = 1; j <= b.length; ++j) // If i == 0 the distance is j, and (0, 0) is already set.
			d[0][j] = j;
		for (int i = 0; i < root.children.length; ++i) {
			a[0] = root.children_idx[i];
			_find(ret, root.children[i], d, a, b, 1, b.length, dist, pfx);
		}
		return ret;
	}

	/* Find but stop searching when enough items are found. */
	public LinkedList<Result> find(String s, int maxd, boolean pfx, int limit) {
		LinkedList<Result> ret = new LinkedList<Result>();
		for (int i = 0; i <= maxd && ret.size() < limit; ++i)
			ret.addAll(find(s, i, pfx)); // TODO What to do with duplicates due to prefix? Perhaps just document it if sort and filter is to be external to this class.
		Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) { // TODO Do this better.
				return b.score - a.score;
			}
		});
		Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) {
				return a.dist - b.dist;
			}
		});
		/*Collections.sort(ret, new Comparator<Result>() {
			@Override
			public int compare(Result a, Result b) {
				return (a.isPrefix ? 1 : 0) + (b.isPrefix ? -1 : 0);
			}
		});*/
		return ret;
	}
}

public class Dictionary7b {
	private static Dawg dawg = new Dawg();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			int s = 99999999;
			while ((line = br.readLine()) != null)
				dawg.add(line, s--);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		dawg.finish();
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long start = System.nanoTime();
		System.out.println("Map start.");
		loadfile("en_full.txt");
		long end = System.nanoTime();
		System.out.println("Map end t = " + Integer.toString((int) ((end - start) / 1000000L)));
/*		dawg.add("confused");
		dawg.add("confued");
		dawg.add("cling");
		dawg.add("running");
		dawg.add("runner");
		dawg.print("", null, 0);
		if (true) return;*/
		System.out.println("Lookup start.");
		start = System.nanoTime();
		LinkedList<Dawg.Result> cits = dawg.find("confsued", 3, true, 10);
		for (Dawg.Result it : cits)
			System.out.println("lup = " + it.word);
		for (int i = 0; i < 1000; ++i)
			dawg.find("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }), 3, true, 10);

		end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (dawg.find(word, 0, false, 1).size() > 0)
					System.out.println("Valid query.");
				else System.out.println("Nope!");
				LinkedList<Dawg.Result> its = dawg.find(word, 3, false, 10);
				for (Dawg.Result it : its)
					System.out.println("lup = " + it.dist + " " + it.score + " " + it.isPrefix + " " + it.word);
				System.out.println("-- end lup, nres = " + its.size() + " --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
				e.printStackTrace();
			}
		}
	}
}
