import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Math;

// SQLite NOTE need "-classpath sqlite-jdbc-3.27.2.1.jar:." and that jar
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class DictDB {
	public static Connection conn = null; // TODO private

	public static void connect() {
		try {
			// TODO choose filename/location etc
			//Class.forName("org.sqlite.JDBC");
			String url = "jdbc:sqlite:dictionary.db";
			/*Properties config = new Properties();
			config.setProperty("journal_mode", "off");
			config.setProperty("synchronous", "off");
			config.setProperty("locking_mode", "exclusive");
			conn = DriverManager.getConnection(url, config);*/
			conn = DriverManager.getConnection(url);
			System.out.println("Connection to SQLite has been established.");

			// TODO this schema can probably use work
			// TODO "WITHOUT ROWID"? does need sqlite 3.8.2
			String sql = "CREATE TABLE IF NOT EXISTS words (\n"
			/*           + "    id integer PRIMARY KEY,\n"*/
			           + "    key text,\n"
			           + "    word text,\n"
			           + "    freq integer\n"
			           + ")";
			String sql_idx = "CREATE INDEX keys ON words (key)";
			//String sql_jmd = "PRAGMA journal_mode=off";
			//String sql_syn = "PRAGMA synchronous=off";
			//String sql_lock = "PRAGMA locking_mode=exclusive";
			try (Statement stmt = conn.createStatement()) {
				// create a new table
				stmt.execute(sql);
				stmt.execute(sql_idx);
				//stmt.execute(sql_jmd);
				//stmt.execute(sql_syn);
				//stmt.execute(sql_lock);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} finally {
				System.out.println("Table OK.");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void disconnect() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static void add(String key, String word) { // TODO prevent duplicates
		String sql = "INSERT INTO words(key,word,freq) VALUES(?,?,?)";
		try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setString(1, key);
			pstmt.setString(2, word); // TODO TODO prevent duplicates at insert same word
			pstmt.setInt(3, 123); // TODO
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	private static String sql_get = "SELECT word,freq FROM words WHERE key=?";
	private static PreparedStatement pstmt;

	public static ResultSet get(String key) {
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement(sql_get);
			pstmt.setString(1, key);
			rs = pstmt.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}
}

class MHashMap { /* Hash map that ignores key collissions and groups collisions in a linked list. */
	public ResultSet get(String key) { // TODO abstract the pool nonsense
		return DictDB.get(key);
	}

	public void add(String key, String value) {
		DictDB.add(key/* & map_mask*/, value);
	}
}

// TODO remember this does not take into account upper/lowercase
class FuzzyMap {
	private static int maxdist = 3; // TODO make configurable / choose wise defaults
	private static int maxdel = 3;
	private static int minlen = 1;
	private MHashMap map = new MHashMap(); // TODO make configurable

	public class Item {
		public String value;
		public int dist;
		Item next;
	}

	// TODO allow limit items count (may need to look in fget also/instead)
	// TODO sort by popularity
	// TODO maybe do not sort here so we can do weighted distance vs popularity sort
	// TODO how to deal with sorting user items that may be more popular than the normal dictionary?
	private Item _get(String key, String keyOrig, Item ret, int del, int skip) { /* Note that this could return null. */
		/* Convert and sort relevant items from the MHashMap return. */
		ResultSet it = map.get(key);
		try {
			while (it.next()) {
				Item nit = new Item(); /* Copy the item. */
				nit.value = it.getString("word");
				nit.dist = distance(nit.value, keyOrig);
				nit.next = null;
				if (nit.dist > maxdist) /* Skip if DL distance is too far. */
					continue;
				/* Insert before items more distant, we append since we're looping to check for duplicates anyway. */
				if (ret == null) /* No target, place initial item. */
					ret = nit;
				else if (ret.dist > nit.dist) { /* Shortest distance yet, insert at start. */
					nit.next = ret;
					ret = nit;
				} else for (Item c = ret;; c = c.next) { /* Iterate through existing items. */
					if (c.value.equals(nit.value)) /* Stop if exists. */
						break;
					if (c.next == null) { /* This is the last item and ours did not exist, add it. */
						c.next = nit;
						break;
					}
					if (c.next.dist > nit.dist) { /* We know c.next is non-null because above code. */
						nit.next = c.next;
						c.next = nit;
						break;
					}
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		/* Get with deletes in key. */
		int len = key.length();
		if (del == maxdel || len <= minlen)
			return ret;
		for (int i = skip; i < len; ++i) { /* Get with deletes in key. */
			String part = key.substring(0, i) + key.substring(i + 1);
			ret = _get(part, keyOrig, ret, del + 1, i);
		}
		return ret;
	}

	public Item get(String key) {
		return _get(key, key, null, 0, 0);
	}

	private void _add(String key, String value, int del, int skip) {
		map.add(key, value);
		int len = key.length();
		if (del == maxdel || len <= minlen)
			return;
		for (int i = skip; i < len; ++i) { /* Add with deletes in key. */
			String part = key.substring(0, i) + key.substring(i + 1);
			_add(part, value, del + 1, i);
		}
	}

	public void add(String value) {
		_add(value, value, 0, 0);
	}

	/* Calculate restricted (optimal string alignment) Damerau-Levenshtein distance using modified Wagner-Fischer algorithm.
	 *
	 * Restricted  in this context means transpositions only count if there are no subsequent edits on their letters.
	 * For more information see (as loaded on 2019-09-27):
	 *   https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Definition
	 *   https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0
	 */
	private static int distance(String a, String b) { // TODO shud be private
		int la = a.length(), lb = b.length();
		int[][] d = new int[la + 1][lb + 1]; /* We have an extra row and column which is 0 when pretending a and b are 1-indexed. */
		for (int i = 0; i <= la; ++i) /* If j == 0 the distance is i. */
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) /* If i == 0 the distance is j, and (0, 0) is already set. */
			d[0][j] = j;
		for (int i = 1; i <= la; ++i) {
			for (int j = 1; j <= lb; ++j) {
				d[i][j] = d[i - 1][j] + 1;
				d[i][j] = Math.min(d[i][j], d[i][j - 1] + 1);
				d[i][j] = Math.min(d[i][j], d[i - 1][j - 1] + ((a.charAt(i - 1) != b.charAt(j - 1)) ? 1 : 0));
				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1))
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
			}
		}
		return d[la][lb];
	}
}

public class Dictionary2 {
	static FuzzyMap map = new FuzzyMap();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			DictDB.conn.setAutoCommit(false); // TODO elsewhere
			while ((line = br.readLine()) != null) {
				map.add(line);
			}
			DictDB.conn.commit(); // TODO elsewhere, commit more to limit startup memory usage
			DictDB.conn.setAutoCommit(true); // TODO does this even make sense?
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		DictDB.connect(); // TODO vacuum commands / vacuum mode?
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));
		
		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				for (FuzzyMap.Item it = map.get(word); it != null; it = it.next) {
					System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
					if (++i >= 15)
						break;
				}
				System.out.println("--end--");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
