import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class Trie { // TODO rename, add delete function if possible?
	private HashMap<Node, Node> nodes = new HashMap<Node, Node>();
	private String[] strings = new String[0];
	private Node root = new Node();

	class Node {
		private Object[] children = new Object[0];
		private boolean isEnd = false; // TODO just check words list hashmap instead of this?
		private int refc = 0;

		private void incRefc() {
			++refc;
		}

		private void decRefc() {
			if (--refc == 0) {
				if (nodes.get(this) == this) // Do not remove if an equivalent class is mapped.
					nodes.remove(this);
				for (int i = 0; i < children.length; i += 2)
					((Node) children[i + 1]).decRefc(); // TODO warning, recursive satan
			}
		}

		private void setChild(String key, Node value) {
			boolean listed = (nodes.get(this) == this); // TODO do this better
			if (listed)
				nodes.remove(this); // Because hashCode will change.
			value.incRefc(); // Decremented later if we are overwriting the same value, prevents refc falling to 0 in that case.
			for (int i = 0; i < children.length; i += 2) {
				if (children[i].equals(key)) {
					((Node) children[i + 1]).decRefc();
					children[i + 1] = value;
					if (listed) // TODO
						nodes.put(this, this);
					return;
				}
			}
			Object[] nc = new Object[children.length + 2];
			System.arraycopy(children, 0, nc, 0, children.length);
			nc[children.length] = dedup(key);
			nc[children.length + 1] = value;
			children = nc;
			if (listed) // TODO
				nodes.put(this, this);
		}

		private Node getChild(String key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; i += 2)
				if (children[i].equals(key))
					return (Node) children[i + 1];
			return null;
		}

		private void getpfx(HashSet<String> ret, String r) {
			if (isEnd)
				ret.add(r);
			for (int i = 0; i < children.length; i += 2)
				((Node) children[i + 1]).getpfx(ret, r + ((String) children[i]));
		}

		// TODO try/catch to catch StackOverflowException
		// TODO add distance to return values?
		private void find(HashSet<String> ret, String s, String r, int d, int maxd, boolean pfx) {
			String c = ((s.length() > 0) ? s.substring(0, 1) : null); // TODO unicode
			if (c == null) {
				if (pfx)
					getpfx(ret, r);
				else if (isEnd)
					ret.add(r);
			} else {
				Node n = getChild(c);
				if (n != null)
					n.find(ret, s.substring(1), r + c, d, maxd, pfx); // TODO unicode
			}
			if (d < maxd) {
				for (int i = 0; i < children.length; i += 2) {
					if (c == null || !c.equals((String) children[i])) {
						// To find words with a delete in the query try everything with any extra char.
						((Node) children[i + 1]).find(ret, s, r + ((String) children[i]), d + 1, maxd, pfx);
						// To find words with a substitution in the query try everything with different char.
						if (c != null)
							((Node) children[i + 1]).find(ret, s.substring(1), r + ((String) children[i]), d + 1, maxd, pfx);
					}
				}
				// TODO some of these are redundant when one delete and one insert is same as substitute
				// To find words with an insertion in the query try with a delete in the query.
				if (c != null)
					find(ret, s.substring(1), r, d + 1, maxd, pfx); // TODO unicode
				// Try with a transpose in the string too.
				if (s.length() >= 2)
					find(ret, s.substring(1, 2) + s.substring(0, 1) + s.substring(2), r, d + 1, maxd, pfx); // TODO unicode
			}
		}

		private HashSet<String> find(String s, int maxd, boolean pfx, int limit) {
			HashSet<String> ret = new HashSet<String>();
			for (int i = 0; i <= maxd && ret.size() < limit; ++i)
				find(ret, s, "", 0, i, pfx); // TODO there oughta be a better way than stepping this? performance seems aight tho
			return ret;
		}

		public Node copy() {
			Node ret = new Node();
			ret.children = new Object[children.length];
			System.arraycopy(children, 0, ret.children, 0, children.length);
			ret.isEnd = isEnd;
			for (int i = 0; i < children.length; i += 2)
				((Node) children[i + 1]).incRefc();
			return ret;
		}

		@Override
		public int hashCode() {
			int ret = 0;
			for (Object i : children) // Must be order-independent!
				ret ^= System.identityHashCode(i);
			return ret ^ Boolean.hashCode(isEnd);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			Node n = (Node) o;
			if (isEnd != n.isEnd)
				return false;
			if (children.length != n.children.length)
				return false;
			for (int i = 0; i < children.length; i += 2) { // Compares array regardless of order.
				boolean found = false;
				for (int j = 0; j < n.children.length; j += 2)
					if (children[i] == n.children[j] && children[i + 1] == n.children[j + 1])
						found = true;
				if (!found)
					return false;
			}
			return true;
		}
	}

	private String dedup(String s) { // This is for deduplicating the index strings.
		for (String i : strings)
			if (i.equals(s))
				return i;
		String[] ns = new String[strings.length + 1];
		System.arraycopy(strings, 0, ns, 0, strings.length);
		ns[strings.length] = s;
		strings = ns;
		return s;
	}

	/* Insert similarly to https://doi.org/10.1016/S0304-3975(02)00571-6 */
	/* But with hashmap to speed up finding similar nodes. */
	public void add(String str) {
		String s = str;
		Node n = root;
		ArrayList<Node> st = new ArrayList<Node>();
		/* Split off existing nodes and create new ones. */
		while (s.length() > 0) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			Node nn = n.getChild(c);
			if (nn == null) {
				nn = new Node();
				n.setChild(c, nn);
				n = nn;
			} else {
				if (nodes.get(nn) == null) // TODO this only for debug
					System.out.println("waaat " + nn + " " + nn.children.length + " " + nn.refc);
				nn = nn.copy(); // TODO maybe no need to copy if refc == 1
				n.setChild(c, nn);
				n = nn;
			}
			st.add(n);
		}
		n.isEnd = true;
		/* In reverse order, replace them with equivalent nodes. */
		for (int i = st.size() - 1; i >= 0; --i) {
			Node on = st.get(i);
			n = nodes.get(on);
			if (n != null) {
				Node p = ((i > 0) ? st.get(i - 1) : root); // TODO maybe just have root in st and solve it that way?
				p.setChild(str.substring(i, i + 1), n); // TODO unicode
			} else nodes.put(on, on); // They're only added to the map here as not to find our newly created ones from here.
			// TODO Better way to keep nodes hashmap in check than above.
		}
		// TODO make sure root node points to the right one
	}

	public boolean contains(String s) {
		Node n = root;
		while (s.length() > 0 && n != null) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			n = n.getChild(c);
		}
		return n != null && n.isEnd;
	}

	public void print(String pfx, Node n, int d) { // TODO only for debug
		if (n == null)
			n = root;
		for (int x = 0; x < d; ++x)
			System.out.print(" ");
		System.out.println(pfx + " " + n.hashCode() + " " + n.isEnd + " " + n.refc + " " + System.identityHashCode(n));
		for (int i = 0; i < n.children.length; i += 2)
			print((String) n.children[i], (Node) n.children[i + 1], d + 1);
	}

	public void info() {
		System.out.println("nodes = " + nodes.size());
	}

	public void finish() { // Stop ability to add but reduce memory use.
		nodes = null;
	}

	public HashSet<String> find(String s, int maxd, boolean pfx, int limit) {
		HashSet<String> ret = root.find(s, maxd, pfx, limit); // TODO 
		return ret; // TODO filter the results with distance function (or no? new search way fairly well limits edit distance but still need sort)
	}
}

public class Dictionary5 {
	private static Trie trie = new Trie();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				trie.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		//trie.finish();
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_full.txt");
		trie.info();
		/*trie.add("wing");
		trie.add("ring");
		trie.add("cling");
		trie.add("running");
		trie.add("runner");
		trie.print("", null, 0);
		if (true) return;*/
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		HashSet<String> cits = trie.find("confsued", 3, true, 10);
		for (String it : cits)
			System.out.println("lup = " + it);
		for (int i = 0; i < 1000; ++i)
			trie.find("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }), 3, true, 10);
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (trie.contains(word))
					System.out.println("Valid query.");
				else System.out.println("Nope!");
				HashSet<String> its = trie.find(word, 3, true, 10);
				for (String it : its) {
					System.out.println("lup = " + it);
				}
				System.out.println("-- end lup, nres = " + its.size() + " --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
