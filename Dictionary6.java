import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class Trie { // TODO rename, add delete function if possible?
	private HashMap<Node, Node> nodes = new HashMap<Node, Node>();
	private String[] strings = new String[0];
	private Node root = new Node();

	class Node {
		private Object[] children = new Object[0];
		private boolean isEnd = false; // TODO just check words list hashmap instead of this?
		private int refc = 0;

		private void incRefc() {
			++refc;
		}

		private void decRefc() {
			if (--refc == 0) {
				if (nodes.get(this) == this) // Do not remove if an equivalent class is mapped.
					nodes.remove(this);
				for (int i = 0; i < children.length; i += 2)
					((Node) children[i + 1]).decRefc(); // TODO warning, recursive satan
			}
		}

		private void setChild(String key, Node value) {
			boolean listed = (nodes.get(this) == this); // TODO do this better
			if (listed)
				nodes.remove(this); // Because hashCode will change.
			value.incRefc(); // Decremented later if we are overwriting the same value, prevents refc falling to 0 in that case.
			for (int i = 0; i < children.length; i += 2) {
				if (children[i].equals(key)) {
					((Node) children[i + 1]).decRefc();
					children[i + 1] = value;
					if (listed) // TODO
						nodes.put(this, this);
					return;
				}
			}
			Object[] nc = new Object[children.length + 2];
			System.arraycopy(children, 0, nc, 0, children.length);
			nc[children.length] = dedup(key);
			nc[children.length + 1] = value;
			children = nc;
			if (listed) // TODO
				nodes.put(this, this);
		}

		private Node getChild(String key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; i += 2)
				if (children[i].equals(key))
					return (Node) children[i + 1];
			return null;
		}

		private void getpfx(HashSet<String> ret, String r) {
			if (isEnd)
				ret.add(r);
			for (int i = 0; i < children.length; i += 2)
				((Node) children[i + 1]).getpfx(ret, r + ((String) children[i]));
		}

		public Node copy() {
			Node ret = new Node();
			ret.children = new Object[children.length];
			System.arraycopy(children, 0, ret.children, 0, children.length);
			ret.isEnd = isEnd;
			for (int i = 0; i < children.length; i += 2)
				((Node) children[i + 1]).incRefc();
			return ret;
		}

		@Override
		public int hashCode() {
			int ret = 0;
			for (Object i : children) // Must be order-independent!
				ret ^= System.identityHashCode(i);
			return ret ^ Boolean.hashCode(isEnd);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			Node n = (Node) o;
			if (isEnd != n.isEnd)
				return false;
			if (children.length != n.children.length)
				return false;
			for (int i = 0; i < children.length; i += 2) { // Compares array regardless of order.
				boolean found = false;
				for (int j = 0; j < n.children.length; j += 2)
					if (children[i] == n.children[j] && children[i + 1] == n.children[j + 1])
						found = true;
				if (!found)
					return false;
			}
			return true;
		}
	}

	private String dedup(String s) { // This is for deduplicating the index strings.
		for (String i : strings)
			if (i.equals(s))
				return i;
		String[] ns = new String[strings.length + 1];
		System.arraycopy(strings, 0, ns, 0, strings.length);
		ns[strings.length] = s;
		strings = ns;
		return s;
	}

	/* Insert similarly to https://doi.org/10.1016/S0304-3975(02)00571-6 */
	/* But with hashmap to speed up finding similar nodes. */
	public void add(String str) {
		String s = str;
		Node n = root;
		ArrayList<Node> st = new ArrayList<Node>();
		/* Split off existing nodes and create new ones. */
		while (s.length() > 0) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			Node nn = n.getChild(c);
			if (nn == null) {
				nn = new Node();
				n.setChild(c, nn);
				n = nn;
			} else {
				if (nodes.get(nn) == null) // TODO this only for debug
					System.out.println("waaat " + nn + " " + nn.children.length + " " + nn.refc);
				nn = nn.copy(); // TODO maybe no need to copy if refc == 1
				n.setChild(c, nn);
				n = nn;
			}
			st.add(n);
		}
		n.isEnd = true;
		/* In reverse order, replace them with equivalent nodes. */
		for (int i = st.size() - 1; i >= 0; --i) {
			Node on = st.get(i);
			n = nodes.get(on);
			if (n != null) {
				Node p = ((i > 0) ? st.get(i - 1) : root); // TODO maybe just have root in st and solve it that way?
				p.setChild(str.substring(i, i + 1), n); // TODO unicode
			} else nodes.put(on, on); // They're only added to the map here as not to find our newly created ones from here.
			// TODO Better way to keep nodes hashmap in check than above.
		}
		// TODO make sure root node points to the right one
	}

	public boolean contains(String s) {
		Node n = root;
		while (s.length() > 0 && n != null) {
			String c = s.substring(0, 1); // TODO unicode
			s = s.substring(1);
			n = n.getChild(c);
		}
		return n != null && n.isEnd;
	}

	public void print(String pfx, Node n, int d) { // TODO only for debug
		if (n == null)
			n = root;
		for (int x = 0; x < d; ++x)
			System.out.print(" ");
		System.out.println(pfx + " " + n.hashCode() + " " + n.isEnd + " " + n.refc + " " + System.identityHashCode(n));
		for (int i = 0; i < n.children.length; i += 2)
			print((String) n.children[i], (Node) n.children[i + 1], d + 1);
	}

	public void info() {
		System.out.println("nodes = " + nodes.size());
	}

	public void finish() { // Stop ability to add but reduce memory use.
		nodes = null;
	}

	/* Calculate restricted (optimal string alignment) Damerau-Levenshtein distance using modified Wagner-Fischer algorithm.
	 *
	 * Restricted  in this context means transpositions only count if there are no subsequent edits on their letters.
	 * For more information see (as loaded on 2019-09-27):
	 *   https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Definition
	 *   https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0
	 */
	public static int distance(String a, String b) { // TODO shud be private
		int la = a.length(), lb = b.length();
		int[][] d = new int[la + 1][lb + 1]; /* We have an extra row and column which is 0 when pretending a and b are 1-indexed. */
		for (int i = 0; i <= la; ++i) /* If j == 0 the distance is i. */
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) /* If i == 0 the distance is j, and (0, 0) is already set. */
			d[0][j] = j;
		for (int i = 1; i <= la; ++i) {
			for (int j = 1; j <= lb; ++j) {
				d[i][j] = d[i - 1][j] + 1;
				d[i][j] = Math.min(d[i][j], d[i][j - 1] + 1);
				d[i][j] = Math.min(d[i][j], d[i - 1][j - 1] + ((a.charAt(i - 1) != b.charAt(j - 1)) ? 1 : 0));
				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1))
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
			}
		}
		return d[la][lb];
	}

	public HashSet<String> find(String s, int maxd, boolean pfx, int limit) {
		HashSet<String> ret = new HashSet<String>();
		for (int i = 0; i <= maxd && ret.size() < limit; ++i)
			ret = find(s, i, pfx);
		return ret;
	}

	/* Search based on modified Wagner-Fischer algorithm. */
	public HashSet<String> find(String b, int di, boolean pfx) {
		HashSet<String> ret = new HashSet<String>();
		int lb = b.length();
		int[][] d = new int[lb + di + 2][lb + 1]; // la is max b.length() + di.
		for (int i = 0; i <= lb + di + 1; ++i) // If j == 0 the distance is i.
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) // If i == 0 the distance is j, and (0, 0) is already set.
			d[0][j] = j;
		for (int i = 0; i < root.children.length; i += 2)
			_find(ret, (Node) root.children[i + 1], d, (String) root.children[i], b, 1, lb, di, pfx);
		return ret;
	}

	private void _find(HashSet<String> ret, Node n, int[][] d, String a, String b, int la, int lb, int di, boolean pfx) {
		for (int j = 1; j <= lb; ++j) {
			d[la][j] = d[la - 1][j] + 1;
			d[la][j] = Math.min(d[la][j], d[la][j - 1] + 1);
			d[la][j] = Math.min(d[la][j], d[la - 1][j - 1] + ((a.charAt(la - 1) != b.charAt(j - 1)) ? 1 : 0));
			if (la > 1 && j > 1 && a.charAt(la - 1) == b.charAt(j - 2) && a.charAt(la - 2) == b.charAt(j- 1))
				d[la][j] = Math.min(d[la][j], d[la - 2][j - 2] + 1);
		}
		int mindist = di + 1;
		for (int i = la, e = Math.min(lb, la + di); i <= e; ++i)
			mindist = Math.min(mindist, d[la][i]);
		if (mindist <= di) {
			for (int i = 0; i < n.children.length; i += 2)
				_find(ret, (Node) n.children[i + 1], d, a + ((String) n.children[i]), b, la + 1, lb, di, pfx);
		}
		if (d[la][lb] <= di) {
			if (pfx)
				n.getpfx(ret, a); // TODO integrate this and get dist?
			else if (n.isEnd)
				ret.add(a); // TODO keep distance
		}
	}
}

public class Dictionary6 {
	private static Trie trie = new Trie();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				trie.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		//trie.finish();
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long start = System.nanoTime();
		System.out.println("Map start.");
		loadfile("en_full.txt");
		long end = System.nanoTime();
		System.out.println("Map end t = " + Integer.toString((int) ((end - start) / 1000000L)));
		trie.info();
/*		trie.add("confused");
		trie.add("confued");
		trie.add("cling");
		trie.add("running");
		trie.add("runner");
		trie.print("", null, 0);
		if (true) return;*/
		System.out.println("Lookup start.");
		start = System.nanoTime();
		HashSet<String> cits = trie.find("confsued", 3, true, 10);
		for (String it : cits)
			System.out.println("lup = " + it);
		for (int i = 0; i < 1000; ++i)
			trie.find("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }), 3, true, 10);

		end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (trie.contains(word))
					System.out.println("Valid query.");
				else System.out.println("Nope!");
				HashSet<String> its = trie.find(word, 3, true, 10);
				for (String it : its) {
					System.out.println("lup = " + it);
				}
				System.out.println("-- end lup, nres = " + its.size() + " --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
				e.printStackTrace();
			}
		}
	}
}
