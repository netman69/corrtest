import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Arrays;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

@SuppressWarnings("unchecked")
class DedupMap<T> {
	private Object[] data = new Object[256];
	private int size = 0;

	private void resize(int size) {
		Object[] odata = data;
		data = new Object[size];
		for (Object o : odata)
			if (o != null)
				add((T) o);
	}

	private int gethc(T obj) { // Get either the index of existing item or to next empty one.
		int h = Math.abs(obj.hashCode()) % data.length;
		for (; data[h] != null && !data[h].equals(obj); h = (h + 1) % data.length);
		return h;
	}

	public void add(T obj) {
		if ((data.length + 1) * 8 / 10 < size)
			resize(data.length * 2); // Double the array if load factor is over 0.8.
		int h = gethc(obj);
		if (data[h] == null) {
			data[h] = obj;
			++size;
		}
	}

	public T get(T obj) {
		return (T) data[gethc(obj)];
	}

	public void remove(T obj) {
		int hs = gethc(obj), h = (hs + 1) % data.length;
		if (data[hs] != null) {
			data[hs] = null;
			// Make sure all chained items are still reachable.
			for (; data[h] != null; h = (h + 1) % data.length) {
				if (gethc((T) data[h]) == hs) {
					data[hs] = data[h];
					data[h] = null;
					hs = h;
				}
			}
			--size;
		}
	}

	public T dedup(T obj) {
		T ret = get(obj);
		if (ret == null) {
			add(obj);
			return (T) obj;
		}
		return ret;
	}
}

class UCUtils {
	public static String pointToString(int cp) {
		return new String(new int[] { cp }, 0, 1);
	}

	public static int length(String s) {
		return s.codePointCount(0, s.length());
	}

	public static int pointAt(String s, int i) {
		return s.codePointAt(s.offsetByCodePoints(0, i));
	}

	public static String substring(String s, int start) {
		return s.substring(s.offsetByCodePoints(0, start));
	}

	public static String substring(String s, int start, int end) {
		return s.substring(s.offsetByCodePoints(0, start), s.offsetByCodePoints(0, end));
	}
}

class Dawg {
	private DedupMap<Node> nodes = new DedupMap<Node>();
	private DedupMap<String> strings = new DedupMap<String>();
	private Node root = new Node();

	private class Node {
		private Object[] children = new Object[0];
		private boolean isEnd = false; // TODO just check words list hashmap instead of this?
		private int refc = 0;

		private void decRefc() { // Warning, this could potentially overflow the stack (it's recursive).
			if (--refc == 0) {
				if (nodes.get(this) == this) // Do not remove if an equivalent class is mapped.
					nodes.remove(this);
				for (int i = 0; i < children.length; i += 2)
					((Node) children[i + 1]).decRefc();
			}
		}

		private void setChild(String key, Node value) { // Warning, potential stack overflow and this changes the hashCode.
			++value.refc; // If we are overwriting the same value, this is decremented later and prevents refc falling to 0 in that case.
			for (int i = 0; i < children.length; i += 2) {
				if (children[i].equals(key)) {
					((Node) children[i + 1]).decRefc();
					children[i + 1] = value;
					return;
				}
			}
			children = Arrays.copyOf(children, children.length + 2);
			children[children.length - 2] = strings.dedup(key);
			children[children.length - 1] = value;
		}

		private Node getChild(String key) { // The traditional way takes 9 months but this one is usually faster.
			for (int i = 0; i < children.length; i += 2)
				if (children[i].equals(key))
					return (Node) children[i + 1];
			return null;
		}

		private Node copy() {
			Node ret = new Node();
			ret.children = Arrays.copyOf(children, children.length);
			ret.isEnd = isEnd;
			for (int i = 0; i < children.length; i += 2)
				++((Node) children[i + 1]).refc;
			return ret;
		}

		private void getPfx(HashSet<String> ret, String r) { // Warning, potential stack overflow.
			if (isEnd)
				ret.add(r);
			for (int i = 0; i < children.length; i += 2)
				((Node) children[i + 1]).getPfx(ret, r + ((String) children[i]));
		}

		@Override
		public int hashCode() {
			int ret = 0;
			for (Object i : children) // Must be order-independent!
				ret ^= System.identityHashCode(i);
			return ret ^ Boolean.hashCode(isEnd);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			Node n = (Node) o;
			if (isEnd != n.isEnd)
				return false;
			if (children.length != n.children.length)
				return false;
			for (int i = 0; i < children.length; i += 2) { // Compares array regardless of order.
				boolean found = false;
				for (int j = 0; j < n.children.length; j += 2)
					if (children[i] == n.children[j] && children[i + 1] == n.children[j + 1])
						found = true;
				if (!found)
					return false;
			}
			return true;
		}
	}

	/* Insert similarly to https://doi.org/10.1016/S0304-3975(02)00571-6 */
	/* But with DedupMap to speed up finding similar nodes. */
	public void add(String str) {
		ArrayList<Node> st = new ArrayList<Node>();
		String s = str;
		Node n = root;
		/* Split off existing nodes and create new ones. */
		st.add(n);
		while (UCUtils.length(s) > 0) {
			String c = UCUtils.substring(s, 0, 1);
			Node nn = n.getChild(c);
			s = UCUtils.substring(s, 1);
			if (nn == null) {
				nn = new Node();
				n.setChild(c, nn);
				n = nn;
			} else {
				nn = nn.copy(); // We must copy it, otherwise we will mess up other words.
				n.setChild(c, nn);
				n = nn;
			}
			st.add(n);
		}
		n.isEnd = true;
		/* In reverse order, replace them with equivalent nodes. */
		for (int i = st.size() - 1; i > 0; --i) {
			Node on = st.get(i);
			if ((n = nodes.get(on)) != null) {
				Node p = st.get(i - 1);
				p.setChild(UCUtils.substring(str, i - 1, i), n);
			} else nodes.add(on);
		}
	}

	public void finish() { // Stop ability to add but reduce memory use.
		nodes = null;
	}

	/* Used only by find function. */
	private void _find(HashSet<String> ret, Node n, int[][] d, int[] a, int[] b, int la, int lb, int maxd, boolean pfx) {
		for (int j = 1; j <= lb; ++j) {
			d[la][j] = d[la - 1][j] + 1;
			d[la][j] = Math.min(d[la][j], d[la][j - 1] + 1);
			d[la][j] = Math.min(d[la][j], d[la - 1][j - 1] + ((a[la - 1] != b[j - 1]) ? 1 : 0));
			if (la > 1 && j > 1 && a[la - 1] == b[j - 2] && a[la - 2] == b[j - 1])
				d[la][j] = Math.min(d[la][j], d[la - 2][j - 2] + 1);
		}
		int mindist = maxd + 1;
		for (int i = la, e = Math.min(lb, la + maxd); i <= e; ++i)
			mindist = Math.min(mindist, d[la][i]);
		if (mindist <= maxd) {
			for (int i = 0; i < n.children.length; i += 2) {
				a[la] = UCUtils.pointAt((String) n.children[i], 0);
				_find(ret, (Node) n.children[i + 1], d, a, b, la + 1, lb, maxd, pfx);
			}
		}
		if (d[la][lb] <= maxd) {
			if (pfx)
				n.getPfx(ret, new String(a, 0, a.length)); // TODO integrate this and get dist?
			else if (n.isEnd)
				ret.add(new String(a, 0, a.length)); // TODO keep distance
		}
	}

	/* Search based on modified Wagner-Fischer algorithm. */
	public HashSet<String> find(String s, int maxd, boolean pfx) {
		HashSet<String> ret = new HashSet<String>();
		int lb = UCUtils.length(s);
		int[][] d = new int[lb + maxd + 2][lb + 1]; // la is max b.length() + di.
		int[] ai = new int[lb + maxd + 1];
		int[] bi = new int[lb];
		for (int i = 0; i < lb; ++i)
			bi[i] = UCUtils.pointAt(s, i);
		for (int i = 0; i <= lb + maxd + 1; ++i) // If j == 0 the distance is i.
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) // If i == 0 the distance is j, and (0, 0) is already set.
			d[0][j] = j;
		for (int i = 0; i < root.children.length; i += 2) {
			ai[0] = UCUtils.pointAt((String) root.children[i], 0);
			_find(ret, (Node) root.children[i + 1], d, ai, bi, 1, lb, maxd, pfx);
		}
		return ret;
	}

	/* Find but stop searching when enough items are found. */
	public HashSet<String> find(String s, int maxd, boolean pfx, int limit) {
		HashSet<String> ret = new HashSet<String>(); // TODO sorting, make more flexible
		for (int i = 0; i <= maxd && ret.size() < limit; ++i)
			ret = find(s, i, pfx);
		return ret;
	}
}

public class Dictionary7 {
	private static Dawg dawg = new Dawg();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null)
				dawg.add(line);
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
		//dawg.finish();
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long start = System.nanoTime();
		System.out.println("Map start.");
		loadfile("en_full.txt");
		long end = System.nanoTime();
		System.out.println("Map end t = " + Integer.toString((int) ((end - start) / 1000000L)));
/*		dawg.add("confused");
		dawg.add("confued");
		dawg.add("cling");
		dawg.add("running");
		dawg.add("runner");
		dawg.print("", null, 0);
		if (true) return;*/
		System.out.println("Lookup start.");
		start = System.nanoTime();
		HashSet<String> cits = dawg.find("confsued", 3, true, 10);
		for (String it : cits)
			System.out.println("lup = " + it);
		for (int i = 0; i < 1000; ++i)
			dawg.find("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }), 3, true, 10);

		end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				if (dawg.find(word, 0, false, 1).size() > 0)
					System.out.println("Valid query.");
				else System.out.println("Nope!");
				HashSet<String> its = dawg.find(word, 3, true, 10);
				for (String it : its) {
					System.out.println("lup = " + it);
				}
				System.out.println("-- end lup, nres = " + its.size() + " --");
			} catch (Exception e) {
				System.out.println("ERRRRROR");
				e.printStackTrace();
			}
		}
	}
}
