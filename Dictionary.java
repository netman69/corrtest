import java.lang.Runtime; // TODO remove, is to see memory usage
import java.io.InputStreamReader; // TODO this is just for debug
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.lang.Math;

// Only for file stuff
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

class LLPool { /* Should be mostly self-explanatory, -1 means no item. */
	// TODO delete option, use -2 in next to mark deleted items or so?
	private static int block_size = 2 << 16, pos = 0;
	private ArrayList<Block> blocks = new ArrayList<Block>();

	private class Block {
		public Object[] data = new Object[block_size]; // TODO maybe use ints indexing into an array to save memory
		public int[] next = new int[block_size];
	}

	public Object get(int i) {
		Block blk = blocks.get(i / block_size);
		if (blk == null)
			return null;
		return blk.data[i % block_size];
	}

	public int getnext(int i) {
		Block blk = blocks.get(i / block_size);
		if (blk == null)
			return -1;
		return blk.next[i % block_size];
	}

	public void set(int i, Object v) {
		Block blk = blocks.get(i / block_size);
		if (blk == null)
			return; // TODO maybe throw error
		blk.data[i % block_size] = v;
	}

	public void setnext(int i, int n) {
		Block blk = blocks.get(i / block_size);
		if (blk == null)
			return; // TODO maybe throw error
		blk.next[i % block_size] = n;
	}

	public int add(Object v) {
		Block blk;
		if (blocks.size() <= pos / block_size)
			blocks.add(new Block());
		blk = blocks.get(pos / block_size);
		blk.data[pos % block_size] = v;
		blk.next[pos % block_size] = -1;
		return pos++;
	}
}

class MHashMap { /* Hash map that ignores key collissions and groups collisions in a linked list. */
	private int map_size, map_mask;
	private int[] map;
	public LLPool pool; // TODO private and abstract this

	public MHashMap(int size) { /* The size argument is # of bits for hashes. */
		map_size = 1 << size; /* Must be a power of 2. */
		map_mask = map_size - 1;
		map = new int[map_size];
		Arrays.fill(map, -1);
		pool = new LLPool();
	}

	public int get(String key) { // TODO abstract the pool nonsense
		return map[key.hashCode() & map_mask];
	}

	public void add(String key, String value) {
		int n = key.hashCode() & map_mask;
		int it = pool.add(value);
		if (map[n] == -1) /* No item for hash, place initial item. */
			map[n] = it;
		else for (int c = map[n];; c = pool.getnext(c)) { /* Iterate through existing items. */
			if (pool.get(c).equals(value)) /* Stop if exists. */
				break;
			if (pool.getnext(c) == -1) { /* This is the last item and ours did not exist, append. */
				pool.setnext(c, it);
				break;
			}
		}
	}
}

// TODO remember this does not take into account upper/lowercase
class FuzzyMap {
	private static int maxdist = 3; // TODO make configurable / choose wise defaults
	private static int maxdel = 3;
	private static int minlen = 1;
	private MHashMap map = new MHashMap(20); // TODO make configurable

	public class Item {
		public String value;
		public int dist;
		Item next;
	}

	private static HashSet<String> _getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		int[] its = new int[depth];
		while (its[depth - 1] + depth - 1 < str.length()) {
			String s = str;
			for (int i = 0; i < depth; ++i) /* Note that the order matters. */
				s = s.substring(0, its[i]) + s.substring(its[i] + 1);
			ret.add(s);
			++its[0];
			for (int i = 1; i < depth; ++i) {
				if (its[i] < its[i - 1]) {
					its[i - 1] = 0;
					++its[i];
				} else break;
			}
		}
		return ret;
	}

	private static HashSet<String> getpartials(String str, int depth) {
		HashSet<String> ret = new HashSet<String>();
		for (int i = 1; i <= depth; ++i)
			if (str.length() > i) // TODO maybe consider minlen, or just hardcode this way?
				ret.addAll(_getpartials(str, i));
		return ret;
	}

	// TODO allow limit items count (may need to look in fget also/instead)
	// TODO sort by popularity
	// TODO maybe do not sort here so we can do weighted distance vs popularity sort
	// TODO how to deal with sorting user items that may be more popular than the normal dictionary?
	private Item _get(String key, String keyOrig, Item ret) { /* Note that this could return null. */
		/* Convert and sort relevant items from the MHashMap return. */
		for (int it = map.get(key); it != -1; it = map.pool.getnext(it)) {
			Item nit = new Item(); /* Copy the item. */
			nit.value = (String) map.pool.get(it);
			nit.dist = distance(nit.value, keyOrig);
			nit.next = null;
			if (nit.dist > maxdist) /* Skip if DL distance is too far. */
				continue;
			/* Insert before items more distant, we append since we're looping to check for duplicates anyway. */
			if (ret == null) /* No target, place initial item. */
				ret = nit;
			else if (ret.dist > nit.dist) { /* Shortest distance yet, insert at start. */
				nit.next = ret;
				ret = nit;
			} else for (Item c = ret;; c = c.next) { /* Iterate through existing items. */
				if (c.value.equals(nit.value)) /* Stop if exists. */
					break;
				if (c.next == null) { /* This is the last item and ours did not exist, add it. */
					c.next = nit;
					break;
				}
				if (c.next.dist > nit.dist) { /* We know c.next is non-null because above code. */
					nit.next = c.next;
					c.next = nit;
					break;
				}
			}
		}
		return ret;
	}

	public Item get(String key) {
		Item ret = _get(key, key, null);
		HashSet<String> keys = getpartials(key, maxdel);
		for (String skey : keys)
			ret = _get(skey, key, ret);
		return ret;
	}

	public void add(String value) {
		map.add(value, value);
		HashSet<String> keys = getpartials(value, maxdel);
		for (String key : keys)
			map.add(key, value);
	}

	/* Calculate restricted (optimal string alignment) Damerau-Levenshtein distance using modified Wagner-Fischer algorithm.
	 *
	 * Restricted  in this context means transpositions only count if there are no subsequent edits on their letters.
	 * For more information see (as loaded on 2019-09-27):
	 *   https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Definition
	 *   https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0
	 */
	private static int distance(String a, String b) { // TODO shud be private
		int la = a.length(), lb = b.length();
		int[][] d = new int[la + 1][lb + 1]; /* We have an extra row and column which is 0 when pretending a and b are 1-indexed. */
		for (int i = 0; i <= la; ++i) /* If j == 0 the distance is i. */
			d[i][0] = i;
		for (int j = 1; j <= lb; ++j) /* If i == 0 the distance is j, and (0, 0) is already set. */
			d[0][j] = j;
		for (int i = 1; i <= la; ++i) {
			for (int j = 1; j <= lb; ++j) {
				d[i][j] = d[i - 1][j] + 1;
				d[i][j] = Math.min(d[i][j], d[i][j - 1] + 1);
				d[i][j] = Math.min(d[i][j], d[i - 1][j - 1] + ((a.charAt(i - 1) != b.charAt(j - 1)) ? 1 : 0));
				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1))
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
			}
		}
		return d[la][lb];
	}
}

public class Dictionary {
	static FuzzyMap map = new FuzzyMap();

	private static void loadfile(String fn) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fn)))) {
			String line;
			while ((line = br.readLine()) != null) {
				map.add(line);
			}
		} catch (Exception e) {
			// TODO Do something!
			System.out.println("ERRRRROR");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		long overhead = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		loadfile("en_part.txt");
		System.out.println("Lookup start.");
		long start = System.nanoTime();
		for (FuzzyMap.Item it = map.get("confsued"); it != null; it = it.next) {
			System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
		}
		for (int i = 0; i < 1000; ++i) {
			map.get("confsued" + new String(new char[] { (char) (i % 26 + (int) 'a') }));
		}
		long end = System.nanoTime();
		System.out.println("ttime = " + Integer.toString((int) ((end - start) / 1000000L)));
		System.out.println("overhead = " + Integer.toString((int) overhead / 1024));
		System.out.println("memuse = " + Integer.toString((int) ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024)));

		while (true) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				String word = reader.readLine();
				int i = 0;
				for (FuzzyMap.Item it = map.get(word); it != null; it = it.next) {
					System.out.println("lup = " + Integer.toString(it.dist) + " " + it.value);
					if (++i >= 15)
						break;
				}
			} catch (Exception e) {
				System.out.println("ERRRRROR");
			}
		}
	}
}
