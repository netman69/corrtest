#include <stdint.h> /* uint64_t */
#include <stddef.h> /* size_t, off_t */
#include <stdlib.h> /* malloc(), NULL */
#include <string.h> /* memcpy(), memset(), strcmp() */

typedef struct {
	char *key;
	void *data;
} mhm_item_t;

typedef struct {
	mhm_item_t *items;
	size_t nitems, size;
} mhm_t;

extern size_t mhm_hash(mhm_t *m, char *key);
extern void mhm_create(mhm_t *m, int size);
extern void mhm_resize(mhm_t *m, int size);
extern void mhm_set(mhm_t *m, char *key, void *data);
extern void *mhm_get(mhm_t *m, char *key);
extern void mhm_del(mhm_t *m, char *key);

size_t mhm_hash(mhm_t *m, char *key) {
	uint64_t ret = 0xCBF29CE484222325;
	char *kp = key;
	while (*kp != 0) /* This is FNV-1A. */
		ret = (ret ^ *(kp++)) * 0x100000001B3;
	ret %= m->size;
	while (m->items[ret].key != NULL) {
		if (strcmp(key, m->items[ret].key) == 0)
			break;
		ret = (ret + 1) % m->size;
	}
	return ret;
}

/* Initial size must be at least 1. */
void mhm_create(mhm_t *m, int size) {
	m->size = size;
	m->items = malloc(m->size * sizeof(mhm_item_t));
	m->nitems = 0;
	memset(m->items, 0, m->size * sizeof(mhm_item_t));
}

void mhm_resize(mhm_t *m, int size) {
	int i;
	mhm_t o;
	memcpy(&o, m, sizeof(mhm_t));
	mhm_create(m, size);
	for (i = 0; i < o.size; ++i)
		if (o.items[i].key != NULL)
			memcpy(&m->items[mhm_hash(m, o.items[i].key)], &o.items[i], sizeof(mhm_item_t));
	m->nitems = o.nitems;
	free(o.items);
}

/* The key must be preserved in memory. */
void mhm_set(mhm_t *m, char *key, void *data) {
	size_t idx = mhm_hash(m, key);
	if (m->items[idx].key == NULL)
		++m->nitems;
	m->items[idx].key = key;
	m->items[idx].data = data;
	/* Double the size when load factor is 0.8. */
	if (m->nitems >= m->size * 8 / 10)
		mhm_resize(m, m->size * 2);
}

void *mhm_get(mhm_t *m, char *key) {
	return m->items[mhm_hash(m, key)].data;
}

void mhm_del(mhm_t *m, char *key) {
	size_t idx = mhm_hash(m, key), it;
	if (m->items[idx].key != NULL) {
		memset(&m->items[idx], 0, sizeof(mhm_item_t));
		--m->nitems;
		for (it = (idx + 1) % m->size; m->items[it].key != NULL; ++it) {
			if (mhm_hash(m, m->items[it].key) == idx) {
				memcpy(&m->items[idx], &m->items[it], sizeof(mhm_item_t));
				memset(&m->items[it], 0, sizeof(mhm_item_t));
				idx = it;
			}
		}
		/* Halve the size when load factor is 0.2. */
		if (m->nitems <= m->size * 2 / 10)
			mhm_resize(m, m->size * 2);
	}
}

#include <stdio.h>

int main(int argc, char *argv[]) {
	mhm_t map;
	mhm_create(&map, 1);
	mhm_set(&map, "test", "hallo");
	mhm_set(&map, "pest", "best");
	mhm_set(&map, "rest", "abcd");
	mhm_set(&map, "zest", "eeee");
	mhm_set(&map, "zest", "eeee");
	mhm_set(&map, "zest", "eooe");
	mhm_del(&map, "rest");

	printf("test = %s\n", mhm_get(&map, "test"));
	printf("pest = %s\n", mhm_get(&map, "pest"));
	printf("rest = %s\n", mhm_get(&map, "rest"));
	printf("zest = %s\n", mhm_get(&map, "zest"));
	printf("west = %s\n", mhm_get(&map, "west"));
	printf("size %lu, nitems %lu, sizeof %lu\n", map.size, map.nitems, sizeof(mhm_item_t));

	return 0;
}
